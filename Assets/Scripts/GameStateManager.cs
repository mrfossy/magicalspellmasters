using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStateManager : MonoBehaviour
{
    [SerializeField] private int playerHealth = 30;
    private int playerArmour;
    [SerializeField] private int enemyHealth = 20;
    
    [SerializeField] private Text enemyHealthText;
    [SerializeField] private Text playerArmourText;
    [SerializeField] private Text playerHealthText;
    [SerializeField] private RectTransform playerSprite;
    [SerializeField] private RectTransform enemySprite;

    //Create states as an enum
    private enum State { MainMenu, PlayerTurn, EnemyTurn };
    private State state;

    IEnumerator MainMenuState()
    {
        //Debug.Log("Enter: Main Menu state");

        while (state == State.MainMenu)
        {
            yield return 0;
        }

        //Debug.Log("Exit: Main Menu state");
    }

    IEnumerator PlayerTurnState()
    {
        //Debug.Log("Enter: Player Turn state");
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        while (state == State.PlayerTurn)
        {
            yield return 0;
        }

        //Debug.Log("Exit: Player Turn state");
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    IEnumerator EnemyTurnState()
    {
        //Debug.Log("Enter: Enemy Turn state");

        while (state == State.EnemyTurn)
        {
            yield return 0;
        }

        //Debug.Log("Exit: Enemy Turn state");
    }


    private void Awake()
    {
        playerHealthText.text = playerHealth.ToString();
        enemyHealthText.text = enemyHealth.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        state = State.PlayerTurn;
        StartCoroutine(PlayerTurnState());
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.MainMenu:

/*                if (Input.GetButtonDown("Cancel") && !gameFinished)
                {
                    state = State.Playing;
                    StartCoroutine(PlayingState());
                }*/

                break;

            case State.PlayerTurn:

                break;

            case State.EnemyTurn:

                break;

            default:
                Debug.Log("Default case: you shouldn't be here.");
                break;
        }
    }

    public void EndPlayerTurn(int numberOfLetters, int letterScore, bool wordIsSpell)
    {
        playerArmour = numberOfLetters;
        playerArmourText.text = playerArmour.ToString();
        
        if (wordIsSpell)
        {
            enemyHealth -= letterScore;
            enemyHealthText.text = enemyHealth.ToString();
        }


        state = State.EnemyTurn;
        StartCoroutine(EnemyTurnState());
    }
}
