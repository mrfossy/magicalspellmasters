using UnityEngine;

 public class FormattingTest : MonoBehaviour
 {
     [Space(5)]
     [Header("Spawner Settings")]
     [Space(15)]
     [SerializeField] private float spawnCooldown;
     [SerializeField] private int numberOfEnemies;

     [Space(5)]
     [Header("Agent Settings")]
     [Space(15)]
     [SerializeField] private string agentName;
     [SerializeField] private float agentSpeed;
 }
