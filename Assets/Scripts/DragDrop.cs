//From tutorial: https://www.youtube.com/watch?app=desktop&v=BGr-7GZJNXg

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private AudioSource[] audioSources;
    private Vector2 preDragPosition;   //the initial position of the tile, prior to dragging
    private bool inDropZone = false;    //tracks whether or not the tile is in a drop zone

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        audioSources = GetComponents<AudioSource>();
        canvas = GetComponentInParent<Canvas>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        if (inDropZone)
        {
            this.GetComponent<RectTransform>().SetParent(GameObject.FindWithTag("LetterZone").transform, false);
            inDropZone = false;
            audioSources[2].Play();

            Text tileLetter = GetComponentInChildren<Text>();
            LetterManager letterManager = GameObject.FindWithTag("LetterManager").GetComponent<LetterManager>();
            letterManager.RemoveFromActiveLetters(this.gameObject);
            //letterManager.SubtractLetterScore(tileLetter.text);
            letterManager.CheckActiveLetters();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("OnDrag");
        if (!inDropZone)
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
            
        //delta is the amount the mouse moved since the previous frame
        //scale factor takes into account the scale of the canvas (to allow the object to track the mouse correctly)
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("BeginDrag");
        if (!inDropZone)
        {
            preDragPosition = rectTransform.anchoredPosition;  //store the initial position of the tile
            canvasGroup.alpha = 0.6f;
            canvasGroup.blocksRaycasts = false; //means raycast will go through this object and land on the drop zone
            audioSources[0].Play();
        }  
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("EndDrag");
        canvasGroup.alpha = 1.0f;
        canvasGroup.blocksRaycasts = true; //resets canvasgroup raycast
        audioSources[1].Play();
        rectTransform.anchoredPosition = preDragPosition;
    }

    public void SetInDropZone(bool inDropZone)
    {
        this.inDropZone = inDropZone;
    }
}
