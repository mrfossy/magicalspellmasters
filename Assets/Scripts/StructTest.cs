using UnityEngine;

public class StructTest : MonoBehaviour
{
    public NiftyStruct[] niftyStructs;
}

//System.Serializable means this will show up in the inspector
[System.Serializable]
public struct NiftyStruct
{
    public string niftyName;
    public float niftyFloat;
    public Color niftyColour;
    public Texture niftyTexture;
    public AudioClip niftyAudioClip;
    public Gradient niftyGradient;
}