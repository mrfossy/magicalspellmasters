using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropZone : MonoBehaviour, IDropHandler
{
    private AudioSource dropSound;

    private void Awake()
    {
        dropSound = GetComponent<AudioSource>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("OnDrop");
        //eventData.pointerDrag is the game object currently being dragged
        if (eventData.pointerDrag != null)
        {
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            eventData.pointerDrag.GetComponent<RectTransform>().SetParent(this.transform, false);
            DragDrop dragDropScript = eventData.pointerDrag.GetComponent<DragDrop>();
            dragDropScript.SetInDropZone(true);
            dropSound.Play();

            //Text tileLetter = eventData.pointerDrag.GetComponentInChildren<Text>();
            LetterManager letterManager = GameObject.FindWithTag("LetterManager").GetComponent<LetterManager>();
            letterManager.AddToActiveLetters(eventData.pointerDrag);
            letterManager.CheckActiveLetters();
        }
    }
}
