//Currently using https://github.com/dwyl/english-words (copyright? has weird mispellings, e.g. 'tecnology')
//Also check out https://github.com/jeremy-rifkin/Wordlist

//Rather than just adding the score as player armour (and potentially allowing the player to amass huge amounts of armour),
//perhaps armour can be calculated as the number of tiles in the submitted word (ie. longer words = more armour).
//In this case, the enemies might only damage by small amounts (e.g. 1 - 4), chipping away at your finite health pool unless you guard against it.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LetterManager : MonoBehaviour
{
    [SerializeField] private GameObject letterTilePrefab;
    [SerializeField] private RectTransform letterZone;
    [SerializeField] private GameObject canvas;
    private int numberOfLetters = 16;
    public int unusedLetters;  //the number of tiles remaining in the letter zone after dragging into active zone or bank
    [SerializeField] private int minWordLength = 1;

    public List<string> scrabbleTiles = new List<string>() {"A", "A", "A", "A", "A", "A", "A", "A", "A",
                                                "B", "B",
                                                "C", "C",
                                                "D", "D", "D", "D",
                                                "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
                                                "F", "F",
                                                "G", "G", "G",
                                                "H", "H",
                                                "I", "I", "I", "I", "I", "I", "I", "I", "I",
                                                "J",
                                                "K",
                                                "L", "L", "L", "L",
                                                "M", "M",
                                                "N", "N", "N", "N", "N", "N",
                                                "O", "O", "O", "O", "O", "O", "O", "O",
                                                "P", "P",
                                                "Q",
                                                "R", "R", "R", "R", "R", "R",
                                                "S", "S", "S", "S",
                                                "T", "T", "T", "T", "T", "T",
                                                "U", "U", "U", "U",
                                                "V", "V",
                                                "W", "W",
                                                "X",
                                                "Y", "Y",
                                                "Z"};

    private string[] alphabet = new string[26] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    private int[] letterScores = new int[26] { 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 };

    public List<GameObject> activeLetters = new List<GameObject>();     //letters currently in the spell drop zone
    public List<string> bankedLetters = new List<string>();    //letters currently stored in player's bank; CHANGE TO GAMEOBJECT LIST

    //private Dictionary<String,String> wordList = new Dictionary<String,String>();
    //private Hashtable wordList = new Hashtable();

    private Dictionary<string, string> wordList = new Dictionary<string, string>();

    //Offensive spell words
    private List<string> magicalWords = new List<string>() { "fire", "flame", "blaze", "inferno", "pyre", "spark", "heat", "bonfire", "combustion", "conflagration", "ember", "flare", "hearth", "tinder", "blast", "pyromania", "pyro",
                                                            "water", "flood", "jet", "shower", "stream", "torrent", "rain", "fountain", "waterfall", "downpour", "tide", "squirt",
                                                            "ice", "blizzard", "frost", "sleet", "floe", "glacier", "hail", "hailstone", "icicle", "permafrost",
                                                            "breeze", "wind", "gale", "tornado", "hurricane", "zephyr", "gust", "draft", "typhoon", "whirlwind", "tempest", "flurry", "draught", "cyclone", "air",  
                                                            "poison", "bilge", "acid", "venom", "cyanide", "virus", "germ", "toxin", "blight", "infection", "contagion", "miasma", "bacteria", "slime", "bile" };


    [SerializeField] private GameObject castButton;
    public int letterScore = 0;
    [SerializeField] private GameObject letterScoreText;
    [SerializeField] private GameObject dropZoneStroke; //coloured stroke for drop zone
    [SerializeField] private Color defaultStrokeColour;
    [SerializeField] private Color defensiveStrokeColour;
    [SerializeField] private Color offensiveStrokeColour;

    //[SerializeField] private int playerHealth = 30;
    //[SerializeField] private int playerArmour = 0;
    //[SerializeField] private Text playerHealthText;
    //[SerializeField] private Text playerArmourText;

    //[SerializeField] private int enemyHealth = 50;
    //[SerializeField] private Text enemyHealthText;

    [SerializeField] GameStateManager gameStateManager;
    private bool wordIsSpell = false;


    private void Awake()
    {
        //Clamp minWordLength to prevent empty Dictionary lookups
        if (minWordLength < 1)
            minWordLength = 1;

        unusedLetters = numberOfLetters;

        TextAsset textFile = Resources.Load("words_alpha", typeof(TextAsset)) as TextAsset;
        wordList = textFile.text.Split("\n"[0]).ToDictionary(w => w.Remove(w.Length - 1));  //removes cariage return from the string
        //wordList.Add(textFile.text.Split("\n"[0]), textFile.text.Split("\n"[0]));

        castButton.SetActive(false);
        dropZoneStroke.GetComponent<Image>().color = defaultStrokeColour;
        //playerHealthText.text = playerHealth.ToString();
        //playerArmourText.text = playerArmour.ToString();
        //enemyHealthText.text = enemyHealth.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        DistributeLetters();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void DistributeLetters()
    {
        for (int i = 0; i < numberOfLetters; i++)
        {
            GameObject letterTileObject = Instantiate(letterTilePrefab, letterZone.GetComponent<Transform>());
            Text[] tileLetter = letterTileObject.GetComponentsInChildren<Text>();
            int randomNumber = Random.Range(0, scrabbleTiles.Count); //max exclusive
            tileLetter[0].text = scrabbleTiles[randomNumber];
            tileLetter[1].text = GetLetterScore(tileLetter[0].text);
            scrabbleTiles.Remove(scrabbleTiles[randomNumber]);
        }
    }

    private string GetLetterScore(string letter)
    {
        for (int i = 0; i < 26; i++)
        {
            if (alphabet[i] == letter)
            {
                return letterScores[i].ToString();
            }
        }

        return null;
    }

    public void AddToActiveLetters(GameObject letter)
    {
        activeLetters.Add(letter);
        unusedLetters--;
    }

    public void RemoveFromActiveLetters(GameObject letter)
    {
        activeLetters.Remove(letter);
        unusedLetters++;
    }

    //These should probably take in Gameobjects too
    public void AddToBankedLetters(string letter)
    {
        bankedLetters.Add(letter);
        unusedLetters--;
    }

    public void RemoveFromBankedLetters(string letter)
    {
        bankedLetters.Remove(letter);
        unusedLetters++;
    }

    public void CheckActiveLetters()
    {
        string currentWord = null;
        int wordLength = activeLetters.Count;
        letterScore = 0;

        for (int i = 0; i < activeLetters.Count; i++)
        {
            currentWord += activeLetters[i].GetComponentInChildren<Text>().text.ToLower();
        }

        //You don't want to do this every update; probably can call the function on the drop event
        if (activeLetters.Count >= minWordLength)
        {
            if (wordList.ContainsKey(currentWord))
            {
                Debug.Log("Valid word!");
                castButton.SetActive(true);

                //Tally up the letter score and update the letter score text UI element
                foreach (GameObject letter in activeLetters)
                {
                    for (int i = 0; i < 26; i++)
                    {
                        if (letter.GetComponentInChildren<Text>().text == alphabet[i])
                        {
                            letterScore += letterScores[i];
                        }
                    }
                }

                letterScoreText.GetComponent<Text>().text = letterScore.ToString();
                dropZoneStroke.GetComponent<Image>().color = defensiveStrokeColour;
                wordIsSpell = false;

                //Check to see whether the current word is one of the magical (i.e. offensive) ones
                CheckForMagicWord(currentWord);
            }
            else
            {
                Debug.Log("Invalid word!");
                letterScore = 0;
                castButton.SetActive(false);
                wordIsSpell = false;
                dropZoneStroke.GetComponent<Image>().color = defaultStrokeColour;
            }
        }
        else
        {
            Debug.Log("Not enough letters!");
            castButton.SetActive(false);
            wordIsSpell = false;
            dropZoneStroke.GetComponent<Image>().color = defaultStrokeColour;
            letterScore = 0;
            letterScoreText.GetComponent<Text>().text = letterScore.ToString();
        }
    }

    private void CheckForMagicWord(string word)
    {
        for (int i = 0; i < magicalWords.Count; i++)
        {
            if (magicalWords[i] == word)
            {
                //Debug.Log("CAST SPELL!");
                wordIsSpell = true;
                dropZoneStroke.GetComponent<Image>().color = offensiveStrokeColour;
            }
        }
    }

    private void ResetTiles()
    {
        activeLetters.Clear();
        bankedLetters.Clear();
        unusedLetters = numberOfLetters;
        GameObject[] lettersOnBoard = GameObject.FindGameObjectsWithTag("LetterTiles");

        foreach (GameObject gameObject in lettersOnBoard)
            Destroy(gameObject);

        DistributeLetters();
    }

/*    //Can be used for incrementing and decrementing
    public void ModifyPlayerHealth(int amount)
    {
        playerHealth += amount;
    }*/

/*    //Can be used for incrementing and decrementing
    public void ModifyPlayerArmour(int amount)
    {
        playerArmour += amount;
    }*/

    /*
    //Decrements the letter score as letter tiles are removed (in DragDrop class, on MousePointerUp)
    public void SubtractLetterScore(string letter)
    {
        for (int i = 0; i < 26; i++)
        {
            if (alphabet[i] == letter)
            {
                letterScore -= letterScores[i];
                letterScoreText.GetComponent<Text>().text = letterScore.ToString();
            }
        }
    */

    public void CastSpell()
    {
        castButton.SetActive(false);

        gameStateManager.EndPlayerTurn(activeLetters.Count, letterScore, wordIsSpell);
    }
}