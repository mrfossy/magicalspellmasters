//Based on: https://www.youtube.com/watch?v=CGsEJToeXmA (up to here: https://youtu.be/CGsEJToeXmA?t=517)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlexibleGridLayout : LayoutGroup
{
    [SerializeField] private int rows;
    [SerializeField] private int columns;
    [SerializeField] private Vector2 cellSize;
    [SerializeField] private Vector2 spacing;

    public override void CalculateLayoutInputHorizontal()
    {
        base.CalculateLayoutInputHorizontal();

        //Calculate number of rows and columns; square root of number of children in transform
        float sqrRt = Mathf.Sqrt(transform.childCount);
        rows = Mathf.CeilToInt(sqrRt);
        columns = Mathf.CeilToInt(sqrRt);

        //Get container dimensiona
        float parentWidth = rectTransform.rect.width;
        float parentHeight = rectTransform.rect.height;

        //Define child cell size
        float cellWidth = (parentWidth / (float)columns) - ((spacing.x / (float)columns) * (columns - 1)) - (padding.left / (float)columns) - (padding.right / (float) columns);
        //float cellWidth = (parentWidth / (float)columns) - ((spacing.x / (float)columns) * 2);
        float cellHeight = (parentHeight / (float)rows) - ((spacing.y / (float)rows) * (rows - 1)) - (padding.top / (float)rows) - (padding.bottom / (float)rows);
        //float cellHeight = (parentHeight / (float)rows) - ((spacing.y / (float)rows) * 2);
        cellSize.x = cellWidth;
        cellSize.y = cellHeight;

        //Column and row indexes
        int columnCount = 0;
        int rowCount = 0;

        //Iterate over all the transform's children; find current row index
        for (int i = 0; i < rectChildren.Count; i++)
        {
            rowCount = i / columns;
            columnCount = i % columns;

            //Reference to child object
            var item = rectChildren[i];

            //Reference to a position for object
            var xPos = (cellSize.x * columnCount) + (spacing.x * columnCount) + padding.left;
            var yPos = (cellSize.y * rowCount) + (spacing.y * rowCount) + padding.top;

            SetChildAlongAxis(item, 0, xPos, cellSize.x);
            SetChildAlongAxis(item, 1, yPos, cellSize.y);
        }
    }

    public override void CalculateLayoutInputVertical()
    {
    }

    public override void SetLayoutHorizontal()
    {
    }

    public override void SetLayoutVertical()
    {
    }
}